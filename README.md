# Android Build Environment

A dockerized build environment for android apps, complete with `cordova`.

Taken from [https://ncona.com/2018/07/android-development-with-docker-2/](https://ncona.com/2018/07/android-development-with-docker-2/) and extended.

## Build docker image
`docker build -t android-build-env .`

## Run container
`docker run -it --volume=$(pwd):/opt/workspace android-build-env bash`

## License

Published under the MIT-License, see [LICENSE-MIT.txt](./LICENSE-MIT.txt) file.


## Contact

Your feedback is appreciated, please e-mail me at [alaarmann@gmx.net](mailto:alaarmann@gmx.net)
